import csv
import difflib
import io
import re
from collections import Counter

from ipywidgets import interact, interactive, interactive_output, Text, VBox, Textarea, Layout, HTML
from IPython.display import display, Javascript
import ipywidgets as widgets
import spacy
from spacy import displacy
from spacy.lang.es import Spanish
from wordcloud import WordCloud

nlp = spacy.load("es_core_news_md")


def copy_to_clipboard(interactive_box):
    def _copy_to_clipboard(a):
        text = interactive_box.result
        with interactive_box.out:
            display(
                Javascript(f"navigator.clipboard.writeText(`{text}`)")
            )
    return _copy_to_clipboard


def create_clipboard_box(interactive_box):
    out = interactive_box.out
    button = widgets.Button(
        description='Copiar resultado',
        disabled=False,
        button_style='', 
        tooltip='Copiar resultado al portapapeles',
        icon='clipboard' 
    )
    button.on_click(copy_to_clipboard(interactive_box))
    return VBox(
        [
            interactive_box.children[0],
            HTML("<br>"),
            button,
            HTML("<br>"),
            out
        ]
    )


# Remove punctuation

def display_removed_punctuation(processed_text):
    paragraph_style = (
        "style=\""
        "font-size: 15px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )
    display(
        HTML(
            f"<div {paragraph_style}>"
            f"<p>{processed_text}</p>"
            "</div>"
        )
    )


punctuation_pattern = re.compile(r"[|.,:;¡!¿?()'\"«»\d\[\]\{\}=]+")
def remove_punctuation(input_text):
    if not input_text:
        return ""
    output_text = re.sub(punctuation_pattern, " ", input_text)
    display_removed_punctuation(
        re.sub(
            rf"({punctuation_pattern.pattern})",
            "<span style=\"background-color: rgba(240,0,0,0.5); padding: 2px;\">\\1</span>",
            input_text[:500]
        )
    )
    return output_text


punctuation_text_area_wrapper = interactive(
    remove_punctuation,
    input_text=Textarea(
        value="",
        placeholder="Inserte el texto aquí",
        description="",
        layout=Layout(min_width='50%' , height='120px')
    )
)


# Transform Case

def display_case_diff(input_segment, output_segment):
    paragraph_style = (
        "style=\""
        "font-size: 15px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )

    html_content = f"<div {paragraph_style}>"

    char_diff = difflib.ndiff(a=input_segment, b=output_segment)
    html_string_b = "<p>"
    for tag, _, char in char_diff:
        if tag == "+":
            html_string_b += "<span style=\"background-color: rgba(0,240,0,0.5); padding: 1px 2px;\">{}</span>".format(
                char
            )
        elif tag == " ":
            html_string_b += char
    html_string_b += "</p>"
    html_content += html_string_b
    display(HTML(html_content + "</div>"))


def transform_case(input_text):
    if not input_text:
        return ""
    output_text = input_text.lower()
    display_case_diff(
        input_text[:500], output_text[:500]
    )
    return output_text


lowercaser_text_area_wrapper = interactive(
    transform_case,
    input_text=Textarea(
        value="",
        placeholder="Inserte el texto aquí",
        description="",
        layout=Layout(min_width='50%' , height='120px')
    ),
)


# Tokenization

def display_tokens(tokens):
    paragraph_style = (
        "style=\""
        "font-size: 16px;"
        "display: flex;"
        "flex-wrap: wrap;"
        "row-gap: 15px;"
        "\""
    )
    html = f"<br><p {paragraph_style}>"
    for i, token in enumerate(tokens):
        html += f"""
<span style="border-radius: 5px; background-color: #eee; color: #444; padding: 2px 5px; margin-right: 10px;">
{token}
</span>
        """
    html += "</p>"
    display(HTML(html))


es_tokenizer = Spanish().tokenizer


def tokenize(input_text):
    if not input_text:
        return ""
    input_text = re.sub("\s+", " ", input_text).strip()
    tokens = es_tokenizer(input_text)
    display_tokens(tokens[:50])
    return " ".join(t.text for t in tokens)
    

tokenizer_text_area_wrapper = interactive(tokenize, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Tokenizador: ",
    layout=Layout(width='50%' , height='120px')
))


# Stop words removal

stop_words = nlp.Defaults.stop_words


def display_removed_words(tokens):
    paragraph_style = (
        "style=\""
        "font-size: 18px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )
    label_style = "style=\"background-color: #eee; color: #444; padding: 5px; border-radius: 5px; margin-right: 10px;\""
    html_content = f"<div {paragraph_style}>"
    html_string_a = f"<p style=\"line-height: 2;\"><span {label_style}>original</span>"
    html_string_b = f"<p style=\"margin-top:10px; font-size: 75%; line-height: 2;\"><span {label_style}>sin stop words</span>"
    for token, is_stop_word in tokens:
        if is_stop_word:
            html_string_a += "<span style=\"background-color: rgba(240,0,0,0.5); padding: 2px;\">{}</span> ".format(
                token
            )
        else:
            html_string_a += "<span>{}</span> ".format(token)
            html_string_b += "<span>{}</span> ".format(token)
    html_string_a += "</p>"
    html_string_b += "</p>"
    html_content += html_string_a + html_string_b + "<br>"
    display(HTML(html_content + "</div>"))


def remove_stop_words(input_text):
    if not input_text:
        return
    tokenized_text = input_text.split(" ")
    tokenized_text = [(t, t in stop_words) for t in tokenized_text]
    output_text = " ".join(t for t, is_sw in tokenized_text if not is_sw)
    display_removed_words(tokenized_text[:400])
    return output_text


stop_words_text_area_wrapper = interactive(
    remove_stop_words,
    input_text=Textarea(
        value="",
        placeholder="Inserte el texto aquí",
        description="",
        layout=Layout(width='50%' , height='120px')
    )
)


# Word frequency

colors = [
    "#FFCDD2",
    "#FFDAB9",
    "#FFE0B2",
    "#FFE8B2",
    "#FFEDB3",
    "#F0F4C3",
    "#C8E6C9",
    "#A5D6A7",
    "#81C784",
    "#66BB6A",
]

def display_frequency_counts(tokens):
    paragraph_style = (
        "style=\""
        "font-size: 16px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )
    html_content = f"<div {paragraph_style}>"
    most_frequent, max_frequency = tokens[0]
    for (token, frequency_count), color in zip(tokens, colors):
        width = frequency_count / max_frequency * 400
        html_content += f"<div style=\"margin-bottom: 10px;\"><div style=\"background-color: {color}; color: #444; height: 30px; width: {width}px; padding-left: 10px;\">{frequency_count}</div><span >{token}</span></div>"
    html_content += "</div>"
    display(HTML(html_content))


def get_frequency_counts(input_text):
    if not input_text:
        return ""
    #doc = nlp(input_text)
    #{
    #    nlp.vocab.strings[k]: v
    #    for k, v
    #    in d.count_by(ORTH).items()
    #}
    counts = Counter(input_text.split())
    top_n_tokens = sorted(counts.items(), key=lambda x: x[1], reverse=True)[:10]
    display_frequency_counts(top_n_tokens)

    with io.StringIO() as f:
        csv_writer = csv.writer(f)
        csv_writer.writerows(counts.items())
        csv_str = f.getvalue()
    return csv_str
    

frequency_text_area_wrapper = interactive(
    get_frequency_counts,
    input_text=Textarea(
        value="",
        placeholder="Inserte el texto aquí",
        description="",
        layout=Layout(width='50%' , height='120px')
    )
)


# Word cloud

word_cloud = WordCloud(
    width = 600,
    height = 600,
    background_color ='white',
    min_font_size = 10
)


def create_word_cloud(frequencies):
    frequency_counts = {}
    for row in csv.reader(frequencies.splitlines()):
        frequency_counts[row[0]] = int(row[1])
    if not frequency_counts:
        return
    word_cloud.generate_from_frequencies(frequency_counts)
    display(HTML("<br>"))
    display(word_cloud.to_image())


word_cloud_widget = interactive(
    create_word_cloud,
    frequencies=Textarea(
        value="",
        placeholder="Inserte el texto aquí",
        description="",
        layout=Layout(width='350px' , height='200px')
    )
)


punctuation_remover_widget = create_clipboard_box(punctuation_text_area_wrapper)
lowercaser_widget = create_clipboard_box(lowercaser_text_area_wrapper)
tokenizer_widget = create_clipboard_box(tokenizer_text_area_wrapper)
stop_word_remover_widget = create_clipboard_box(stop_words_text_area_wrapper)
word_frequency_widget = create_clipboard_box(frequency_text_area_wrapper)
