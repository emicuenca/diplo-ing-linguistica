import difflib
import re

from ipywidgets import interact, interactive, Text, Textarea, Layout, HTML
from IPython.display import display
import ipywidgets as widgets
import nltk
from nltk import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer
import spacy
from spacy import displacy
from spacy.lang.es import Spanish

nlp = spacy.load("es_core_news_md")
nltk.download('stopwords')
nltk.download('punkt')


def hex_dump(input_text):
    if not input_text:
        return
    html = "<br><div style=\"font-family:monospace; font-size: 16px; display:flex; column-gap: 20px;\">"
    character_box = """
<div style="display:flex;flex-direction:column; align-items: center; blank-space:pre; padding: 5px; border-radius: 5px; background-color: #ddd; color: #444;">
    <span>{}</span>
    <span>{}</span>
</div>"""
    for c in input_text:
        c_hex = c.encode(encoding="utf-8").hex()
        html += character_box.format(c if c != " " else " ", c_hex)
    html += "</div>"
    display(HTML(html))


hex_dump_widget = interactive(hex_dump, input_text=Text(
    value="",
    placeholder="Inserte el texto aquí",
    description="",
    layout=Layout(width='50%')
))


def segmentation(input_text):
    pattern = r"\.+\s*"
    if not input_text:
        return
    html = "<br>"
    segments = [s for s in re.split(pattern, input_text) if s]
    for i, segment in enumerate(segments):
        html += f"<p style=\"font-size: 16px; margin-bottom: 5px;\"><span style=\"font-weight: bold; padding: 5px; border-radius: 5px; background-color: #eee; color: #444; margin-right: 10px;\">Oración #{i+1}</span> {segment}</p>"
    display(HTML(html))


segmentation_widget = interactive(segmentation, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Segmentador: ",
    layout=Layout(width='50%' , height='120px')
))


def display_tokens(tokens):
    paragraph_style = (
        "style=\""
        "font-size: 16px;"
        "display: flex;"
        "flex-wrap: wrap;"
        "row-gap: 15px;"
        "\""
    )
    html = f"<br><p {paragraph_style}>"
    for i, token in enumerate(tokens):
        html += f"""
<span style="border-radius: 5px; background-color: #eee; color: #444; padding: 2px 5px; margin-right: 10px;">
{token}
</span>
        """
    html += "</p>"
    display(HTML(html))


def tokenizer(input_text):
    pattern = r"\s+"
    if not input_text:
        return
    tokens = [t for t in re.split(pattern, input_text) if t]
    display_tokens(tokens)


basic_tokenizer_widget = interactive(tokenizer, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Tokenizador: ",
    layout=Layout(width='50%' , height='120px')
))


es_tokenizer = Spanish().tokenizer

def tokenizer_2(input_text):
    if not input_text:
        return
    tokens = es_tokenizer(input_text)
    display_tokens(tokens)
    

improved_tokenizer_widget = interactive(tokenizer_2, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Tokenizador: ",
    layout=Layout(width='50%' , height='120px')
))


def display_diff(input_segments, output_segments):
    paragraph_style = (
        "style=\""
        "font-size: 20px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )

    html_content = f"<div {paragraph_style}>"
    for input_segment, output_segment in zip(input_segments, output_segments):
        sm = difflib.SequenceMatcher(a=input_segment, b=output_segment)
        optcodes = sm.get_opcodes()
        html_string_a = "<p>"
        html_string_b = "<p>"
        for tag, a_start, a_end, b_start, b_end in optcodes:
            if tag == 'equal':
                html_string_a += "<span>{}</span>".format(
                    input_segment[a_start: a_end]
                )
                html_string_b += "<span>{}</span>".format(
                    output_segment[b_start: b_end]
                )
            else:
                html_string_a += "<span style=\"background-color: rgba(240,0,0,0.5); padding: 2px;\">{}</span>".format(
                    input_segment[a_start: a_end]
                )
                html_string_b += "<span style=\"background-color: rgba(0,240,0,0.5); padding: 2px;\">{}</span>".format(
                    output_segment[b_start: b_end]
                )
        html_string_a += "</p>"
        html_string_b += "</p>"
        html_content += html_string_a + html_string_b + "<br>"
    display(HTML(html_content + "</div>"))


def transform_case(input_text):
    pattern = r"\.+\s*"
    if not input_text:
        return
    input_segments = [s for s in re.split(pattern, input_text) if s]
    output_segments = [s for s in re.split(pattern, input_text.lower()) if s]
    display_diff(input_segments, output_segments)


case_widget = interactive(transform_case, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="",
    layout=Layout(width='50%' , height='120px')
))


stop_words = set(stopwords.words('spanish'))


def display_removed_words(segments):
    paragraph_style = (
        "style=\""
        "font-size: 18px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "\""
    )
    label_style = "style=\"background-color: #eee; color: #444; padding: 5px; border-radius: 5px; margin-right: 10px;\""
    html_content = f"<div {paragraph_style}>"
    for segment in segments:
        html_string_a = f"<p style=\"line-height: 2;\"><span {label_style}>original</span>"
        html_string_b = f"<p style=\"margin-top:10px; font-size: 75%; line-height: 2;\"><span {label_style}>sin stop words</span>"
        for token, is_stop_word in segment:
            if is_stop_word:
                html_string_a += "<span style=\"background-color: rgba(240,0,0,0.5); padding: 2px;\">{}</span> ".format(
                    token
                )
            else:
                html_string_a += "<span>{}</span> ".format(token)
                html_string_b += "<span>{}</span> ".format(token)
        html_string_a += "</p>"
        html_string_b += "</p>"
        html_content += html_string_a + html_string_b + "<br>"
    display(HTML(html_content + "</div>"))


def stop_word_remover(input_text):
    if not input_text:
        return
    input_text = input_text.lower()
    tokenized_text = [
        word_tokenize(s) for s in sent_tokenize(input_text, language="spanish")
    ]
    segments = [
        [(t, t in stop_words) for t in s]
        for s in tokenized_text
    ]
    display_removed_words(segments)


stop_words_widget = interactive(stop_word_remover, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="",
    layout=Layout(width='50%' , height='120px')
))


def display_table(processed):
    output_style = (
        "style=\""
        "font-size: 18px;"
        "font-family: monospace;"
        "margin-top: 20px;"
        "margin-left: 40px;"
        "display: grid;"
        "grid-template-columns: repeat(2, 400px);"
        "grid-row-gap: 20px;"
        "\""
    )
    html_content = f"<div {output_style}>"
    for token, stemmed_token in processed:
        html_content += (
            f"<span>{token}</span>"
            f"<span>{stemmed_token}</span>"
        )
    html_content += "</div>"
    display(HTML(html_content))


stemmer = SnowballStemmer("spanish")


def stem_list(input_text):
    processed = []
    for token in input_text.splitlines():
        stemmed_token = stemmer.stem(token)
        processed.append((token, stemmed_token))
    display_table(processed)


stemming_widget = interactive(stem_list, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Stemming",
    layout=Layout(width='300px' , height='200px')
))


lemmatizer = WordNetLemmatizer()

def lemmatize_list(input_text):
    processed = []
    for token in nlp(input_text):
        processed.append(
            (token.text, token.lemma_)
        )
    display_table(processed)


lemmatization_widget = interactive(lemmatize_list, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="Lematizador",
    layout=Layout(width='300px' , height='200px')
))


def pos_tagger(input_text):
    if not input_text:
        return
    doc = nlp(input_text)
    html = displacy.render(doc, style="dep", options={"compact": False})
    display(HTML(html))


pos_tagger_widget = interactive(pos_tagger, input_text=Text(
    value="",
    placeholder="Inserte una oración...",
    description="",
    layout=Layout(width='600px' , height='60px'),
    continuous_update=False,
))


def entity_recognizer(input_text):
    if not input_text:
        return
    doc = nlp(input_text)
    html = "<br>" + displacy.render(doc, style="ent")
    display(HTML(html))


entity_recognition_widget = interactive(entity_recognizer, input_text=Textarea(
    value="",
    placeholder="Inserte el texto aquí",
    description="",
    layout=Layout(width='50%' , height='120px')
))
